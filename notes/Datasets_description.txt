Von: "Dullin, Christian" <christian.dullin@med.uni-goettingen.de>
Datum: 18. August 2017 um 15:33:41 MESZ
An: Thomas Rittmann <thomas.rittmann@stud.uni-goettingen.de>
Betreff: AW: DIP-project: Breathing mice
Hallo Thomas,

hier sind die links zu den Datensätzen http://gofile.me/3n8YP/JUFSF96Zb
und http://gofile.me/3n8YP/qcYlDj6xZ

und ein paar informationen: die Daten sind aufgenommen mit 90kV, 40uA, 30 fps und 40um pixel size.
es handelt sich um Daten von zwei Zeitpunkten (before challenge und acute phase)

mit CN sind Kontrolltiere bezeichnet mit SAA (severe allergic asthma) Asthmatiere. Before challenge heisst bevor eine Asthmaattacke ausgelöst wurde (sollten also mehr oder weniger gesund aussehen) und after challenge sollten nur die SAA Tiere und nicht CN Tiere einen Asthmaanfall zeigen.

Für jedes Tier gibt es ein Unterverzeichnis mit 2 Bildern. Das JPG ist nur eine Übersichtsbild. Das TIFF ist ein Multiframe (1024) TIFF Bild.

Lasst mich wissen wenn ihr mehr Informationen braucht.

LG und schönes Wochenende!
Christian
