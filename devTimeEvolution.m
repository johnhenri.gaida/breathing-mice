obj = CTData.loadData('CN', 'BC',2);
%%
% timeEvolution = obj.getTimeEvolution();
[BW,lung_cut]=lung_roi2(obj.MF);
[TW,bg_cut]=bg_roi(obj.MF);
integrated=squeeze(mean(mean(lung_cut)));
bg_integrated=squeeze(mean(mean(bg_cut)));
%%
smoothingWidth=200;
order=4;
roiEvolve = (integrated-bg_integrated)./mean(bg_integrated);
back_tend=smooth(roiEvolve,smoothingWidth);
x=1:length(back_tend);
%% omit the edges of the time evolution
x_short=smoothingWidth/2:(length(back_tend)-smoothingWidth/2);
back_tend_short=back_tend(smoothingWidth/2:(length(back_tend)-smoothingWidth/2));
back_tend_short=back_tend_short';
%% fit polynom
p = polyfit(x_short,back_tend_short,order);
pcorr=p;
pcorr(order+1)=0;
timeEvolution=roiEvolve-polyval(pcorr,x)';
%
figure(2)
plot(back_tend)
hold on
plot(roiEvolve)
plot(polyval(p,x))
plot(timeEvolution)
xlabel('Frames'), ylabel('Intensity (norm.)')
xlim([100 900]);
hold off
