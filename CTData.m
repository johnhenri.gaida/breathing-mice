classdef CTData
    properties (SetAccess = private)
        No          % Number of measurement
        AnimalType  % Control (CN) or severe allergic asthma (SAA)
        PointInTime % Point in time the data was taken - before challenge (BC) or acute phase (AP)
        ImageParam  % Image parameters
    end
    
    properties
        Overview    % overview image
        MF          % multiframe image
        PrettyName  % Pretty sample name
        ROI         % Region of Interest
        StringName  % String name of Animal / Data Type
        FilterType  % Type of Filter applied (String)  Default:Raw
    end
    
    properties (Dependent, SetAccess = private)
        Dim         % Dimension of MF
        Path        % Path to data
    end
    
    properties (Constant)
        CTScanParam = CTScanParam(90, 40)   % CT scan parameters
        MFParam = MFParam(30)               % Multiframe parameters
        PixelSize = 40;                     % Pixelsize (um)
        DirData = './Data';
        PointInTimeArray = containers.Map({'BC', 'AP'}, {'before challenge', 'acute phase'});
    end
    
    methods
        function obj = CTData(no,animalType, pointInTime, imageParam)
            obj.No = no;
            obj.AnimalType = animalType;
            obj.PointInTime = pointInTime;
            obj.ImageParam = imageParam;
            
        end
        function dim = get.Dim(obj)
            dim = size(obj.MF);
        end
        function path = get.Path(obj)
            pitString = obj.PointInTimeArray(obj.PointInTime);
            path = fullfile(obj.DirData, pitString, [obj.AnimalType,sprintf('_%.3d',obj.No)]);
        end
        function str=getString(obj)
            str=sprintf('Mouse %s No. %d - %s',obj.AnimalType,obj.No,obj.PointInTime); %,obj.FilterType
        end
        
        function movplay(obj, colormap)
            if ~exist('colormap','var')
                colormap = @gray;
            end
            implay(CTData.getMov(obj.MF, colormap),obj.MFParam.Fps);
        end
        
        function playMovie(obj, colormap)
            if ~exist('colormap','var')
                colormap = @gray;
            end
            
            N=length(obj);
            for iter=1:N
                imp(iter)=implay(CTData.getMov(obj(iter).MF, colormap),30);
            end
            obj.arrangeWindows();
            for iter=1:N
                play(imp(iter).DataSource.Controls);
            end
        end
        
        
        function obj=setDynRange(obj,min,max)
            obj.MF(obj.MF<min)=min;
            obj.MF(obj.MF>max)=max;
        end
        
        function arrangeWindows(obj)
            N=length(obj);
            mov_handle=findall(0,'tag','spcui_scope_framework');
            screen_size=get(0,'screensize');
            screen_width=screen_size(3);
            screen_height=screen_size(4);
            
            rows = floor(sqrt(N));
            while mod(N,rows) ~= 0
                rows = rows - 1;
            end
            
            cols=N/rows;
            window_width=(screen_width./cols)*0.9;
            window_height=(screen_height./rows)*0.7;
            window_size=min([window_width window_height]);
            
            for iter=1:N
                row_pos=ceil(iter/cols);
                col_pos=iter-((row_pos-1)*cols);
                set(mov_handle(iter),'position',[50+(col_pos-1)*window_size 50+(row_pos-1)*(window_size+150) window_size window_size]);
                set(mov_handle(iter), 'Name', getString(obj(N-iter+1))); %// set title
            end
        end
        
        function [timeEvolution, offset] = getTimeEvolution(obj)       
            [BW,lung_cut]=lung_roi3(obj.MF);
            [TW,bg_cut]=bg_roi3(obj.MF);            
            lung_mean=squeeze(mean(mean(lung_cut)));
            bg_mean=squeeze(mean(mean(bg_cut)));
            roiEvolve = (lung_mean-bg_mean)./mean(bg_mean);
                        
            
            smoothingWidth=200;
            order=4;
            % subtract drift
            back_tend=smooth(roiEvolve,smoothingWidth);
            x=1:length(back_tend);
            x_short=smoothingWidth/2:(length(back_tend)-smoothingWidth/2);
            back_tend_short=back_tend(smoothingWidth/2:(length(back_tend)-smoothingWidth/2));
            back_tend_short=back_tend_short';
            t=BW;
            a=TW;
            p = polyfit(x_short,back_tend_short,order);
            pcorr=p;
            offset = p(order+1);
            pcorr(order+1)=0;
            timeEvolution=roiEvolve-polyval(pcorr,x)';
            %
            %figure;
            %plot(roiEvolve);
            %hold on;
            %plot(back_tend);
            %plot(polyval(p,x)');
            
        end
        
        function [ft, freq] = getFt(obj, timeEvolution)
            ft=abs(fftshift(fft(timeEvolution)));
            ft=ft(length(ft)/2:length(ft));
            dur=length(timeEvolution)/obj.MFParam.Fps;
            df=1/dur;
            freq=0:df:(length(ft)/2)*df;
        end
        
        % --- Data analyzing methods ---
        function harmonicRatio = getHarmonicRatio(obj)
            timeEvolution = obj.getTimeEvolution();
            [ft,freq] = obj.getFt(timeEvolution);
            df = mean(diff(freq));
            % integrate first harmonic peak
            firstPeakInt = getInt(0.64,0.88,freq);
            firstPeak = sum(ft(firstPeakInt)).*df;
            % integrate second harmonic peak
            secondPeakInt = getInt(1.40,1.64,freq);
            secondPeak = sum(ft(secondPeakInt)).*df;
            harmonicRatio = firstPeak/secondPeak;
        end
        
        function offset = getOffset(obj)
            [timeEvolution, offset] = obj.getTimeEvolution();
        end
        
        function diaphragmIntegration = getDiaphragm(obj)
            deviation = std(double(obj.MF),0,3);
            diaphragmIntegration = mean(mean(deviation));
        end
                
        
        function peakH=getPeakH(obj)
            timeEvolution=obj.getTimeEvolution();
            start_cut=100;
            end_cut=length(timeEvolution-start_cut);
            timeEvolution=timeEvolution(start_cut:end_cut);
            [pks, pks_frame]=CTData.getPeaks(timeEvolution,25,0.3);
            %figure;
            %plot(timeEvolution);
            %hold on;
            %scatter(pks_frame,pks);
            %hold off;
            mean_peak=mean(pks);
            mean_all=mean(timeEvolution);
            mean_dif=(mean_peak-mean_all)./mean_all;
            peakH=mean_dif;
        end
        
        
        function quot_med=getQuot(obj,plotting)
            if ~exist('plotting','var')
                plotting = 0;
            end
            timeEvolution=obj.getTimeEvolution();
            oldtime=1:1024;
            finetime=linspace(1,1024,10240);
            start_cut=500;
            end_cut=length(finetime)-start_cut;
            fineTimeEvolution=interp1(oldtime,timeEvolution,finetime);
            timeEvolution=fineTimeEvolution;
            timeEvolution=timeEvolution(start_cut:end_cut);

            [pks, pks_frame]=CTData.getPeaks(timeEvolution,40,0.3);
                        
            double_thresh=200;
            peak_dist=diff(pks_frame);

            betw_ind=pks_frame(1:length(pks_frame)-1)+floor(3*peak_dist/5);    
            ind_close=find(diff(pks_frame)<double_thresh);
            pks(ind_close:ind_close+1)=[];
            pks_frame(ind_close:ind_close+1)=[];
            betw_ind(ind_close:ind_close+1)=[];


            for it=2:length(pks_frame)-1
               before_mean=mean(timeEvolution(betw_ind(it-1)-15:betw_ind(it-1)+15));
               after_mean=mean(timeEvolution(betw_ind(it)-15:betw_ind(it)+15));
               before_thresh=before_mean+0.05*(pks(it)-before_mean);
               after_thresh=after_mean+0.05*(pks(it)-after_mean);
               %max_thresh=max([before_thresh after_thresh]);
               %before_thresh=max_thresh;
               %after_thresh=max_thresh;
               
               % find rise threshold / index
               roi_bef=timeEvolution(betw_ind(it-1):pks_frame(it));
               idx1=roi_bef>=before_thresh;
               idx1(1)=0;
               idx=find(idx1);
               thresh_rel=max(idx(roi_bef(idx-1)<before_thresh));
               if (isempty(thresh_rel))
                   thresh_rel=0;
               end
               thresh_ind(it-1)=betw_ind(it-1)+thresh_rel-1; 
               
               % find fall threshold /index
               roi_aft=timeEvolution(pks_frame(it):betw_ind(it));
               idxa1=roi_aft<=after_thresh;
               idxa1(1)=0;
               idxa=find(idxa1);
               thresh_rel_a=min(idxa(roi_aft(idxa-1)>after_thresh));
               if (isempty(thresh_rel_a))
                   thresh_rel_a=0;
               end
               thresh_ind_a(it-1)=pks_frame(it)+thresh_rel_a-1; 

            end  
           
            
           pks=pks(2:length(pks)-1);
           pks_frame=pks_frame(2:length(pks_frame)-1);
           fall_time=thresh_ind_a-pks_frame;
           rise_time=pks_frame-thresh_ind;
           quot=fall_time./rise_time;
           quot_med=median(quot);
           
           
          % quot_med=mean(quot);
          
            if (plotting)
                fig=figure;
                plot(timeEvolution);
                hold on;
                set(fig,'Position',[100 100 1600 900]);            
                scatter(pks_frame,pks);
                scatter(betw_ind,timeEvolution(betw_ind));
                scatter(thresh_ind,timeEvolution(thresh_ind));
                scatter(thresh_ind_a,timeEvolution(thresh_ind_a));
                title(obj.getString());
                xlabel('Frame (x10)');
                ylabel('Lung brightness (normalized)');
                xlim([0 9100]);
                legend('Time Evolution','Peaks','Valley average','Rise point','Fall point');
            end 
            %figure;
            %hist(quot);
        end
       
        
        
        function test = getTest(obj)
            test=1;
        end
        
    end
    
    methods (Static)
        function obj = loadData(animalType, pointInTime, no)
            warning('off','MATLAB:imagesci:tiffmexutils:libtiffErrorAsWarning');
            imageParam = ImageParam(CTData.PixelSize);
            obj = CTData(no, animalType, pointInTime, imageParam);
            path = obj.Path;
            filesTif=dir(fullfile(path, '*.tif'));
            filesJpg=dir(fullfile(path, '*.jpg'));
            fileName = filesTif(1).name;
            mf = CTData.readMultiframe(fullfile(path,fileName));
            obj.MF = mf;
            obj.MF=flip(obj.MF,1);
            obj.MF=flip(obj.MF,2);
            obj.Overview = imread(fullfile(path,filesJpg(1).name));
            obj.FilterType = 'Raw';
        end
        
        function finalImage = readMultiframe(fileTif)
            % Read in a multiframemov tiff
            % http://www.matlabtips.com/how-to-load-tiff-stacks-fast-really-fast/
            InfoImage=imfinfo(fileTif);
            mImage=InfoImage(1).Width;
            nImage=InfoImage(1).Height;
            NumberImages=length(InfoImage);
            finalImage=zeros(nImage,mImage,NumberImages,'uint16');
            
            TifLink = Tiff(fileTif, 'r');
            for i=1:NumberImages
                TifLink.setDirectory(i);
                finalImage(:,:,i)=TifLink.read();
            end
            TifLink.close();
        end
        
        function converted = convert2uint8(mf)
            minGlobal = min(min(min(mf)));
            maxGlobal = max(max(max(mf)));
            norm = 2^8/double(maxGlobal-minGlobal);
            converted = uint8(double(mf-minGlobal).*norm);
        end
        
        function mov = getMov(mf, colormap)
            % Convert multiframe image in movie
            dim = size(mf);
            if ~isa(mf, 'uint8')
                converted = CTData.convert2uint8(mf);
            else
                converted = mf;
            end
            %             map = repmat(linspace(0,1,2^8)',[1 3]);
            map = colormap(2^8);
            mov = immovie(reshape(converted,[dim(1),dim(2),1,dim(3)]), map);
            
            %clear mov;
            %for ite=1:100
            %    mov(ite)=im2frame(converted(:,:,ite),map);
            % end
            % movlen=100;
            %figc=figure('visible','off');
            %set(figc,'Position',[100 100 1200 600]');
            %mov(movlen) = struct('cdata',[],'colormap',[]);
            %for ite=1:movlen
            %   subplot(1,2,1);
            %  imshow(converted(:,:,ite),[0 255]);
            % title('test1');
            %subplot(1,2,2);
            %imshow(converted(:,:,ite),[0 255]);
            %title('test2');
            %mov(ite)=getframe(figc);
            %end
        end
        
        function result = parameterIterator(animalType, pointInTime, nos, methods)
            n = length(nos);
            m = length(methods);
            result = zeros(n,m);
            for i = 1:n
                no = nos(i);
                obj = CTData.loadData(animalType, pointInTime,no);
                for j = 1:m
                    method = methods{j};
                    result(i,j) = method(obj);
                end
            end
        end
        
        function timeEvolutions = timeEvolutionIterator(animalType, pointInTime, nos)
            m=length(nos);
            n=1024;
            timeEvolutions = zeros(n,m);
            for i = 1:m
                no = nos(i);
                obj = CTData.loadData(animalType, pointInTime,no);
                timeEvolutions(:,i)=obj.getTimeEvolution();
            end
        end
        
        function  [pks,pks_frame]=getPeaks(timeEvolution,minPeakDist,peakToMeanRatio)
            meanSig=mean(timeEvolution);
            maxSig=max(timeEvolution);
            minPeakHeight=peakToMeanRatio*(maxSig-meanSig)+meanSig;
            [pks, pks_frame]=findpeaks(timeEvolution,'MinPeakDistance',minPeakDist,...
                'MinPeakHeight',minPeakHeight);          
        end
    end
end