function [rows, columns] = squaredPlotGrid(n)
rows = floor(sqrt(n));
columns = ceil(n/rows);
end