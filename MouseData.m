classdef MouseData
   
    properties
        AnimalType      % Control (CN) or severe allergic asthma (SAA)
        CTData_before   % CT Data before the challenge
        CTData_acute   % CT Data of acute phase
    end
    
    methods
        function obj=MouseData(no,health)
            obj.CTData_before=CTData(health,'BC',no);
            obj.CTData_acute=CTData(health,'AP',no);
            obj.AnimalType=health;
        end
    end
end

