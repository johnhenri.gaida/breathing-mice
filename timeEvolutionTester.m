n = 9;
animalTypes = {'SAA','SAA','CN'};
pointInTimes = {'BC','AP','AP'};
%nos = [1:3,1,6,8,1,6,8];
nos=[1:3,1:3,1:3];
%%
tic
timeEvolutions(:,1:3) = CTData.timeEvolutionIterator(animalTypes{2},pointInTimes{1},1:3);
timeEvolutions(:,4:6) = CTData.timeEvolutionIterator(animalTypes{2},pointInTimes{2},1:3);
timeEvolutions(:,7:9) = CTData.timeEvolutionIterator(animalTypes{3},pointInTimes{3},1:3);
toc
%%
[rows,columns] = squaredPlotGrid(n);
figure(4)
for i=1:n
    subplot(rows,columns,i), plot((1:1024)./30,timeEvolutions(:,i))
    xlim([0 33]), ylim([2.5 3.2])
    xlabel('Time (s)'), ylabel('Intensity (norm.)')
    ceil(n/3)
    title(sprintf('Mouse %s No. %d - %s',animalTypes{ceil(i/3)},nos(i),CTData.PointInTimeArray(pointInTimes{ceil(i/3)})));
end
%%
saveas(4,'01_all_time_evolutions','png')