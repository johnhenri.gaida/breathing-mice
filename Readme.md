# Breathing Mice - Asthma Detection from Cinematic X-Ray Imaging

Matlab evaluation of cinematic CT data (30 fps 40µm resolution) from the lab of
[Christian Dullin](https://www.researchgate.net/profile/Christian_Dullin).

![Title Image](./images/01_title_image.png)

## Code
`CTData` is the main class holding the data and all other necessary meta information.
Most of the methods are defined in this class.

### Loading Data
```matlab
data = CTData.loadData('CN', 'BC', 1);
```
 Loads the data set for healthy mouse no 1 before the challenge from the folder `./Data/before challenge/CN_001`.

### Displaying videos
```matlab
playMovie(data)
playMovie(data, @jet)
```
Shows the CT movie per default in gray scale or by the specified colormap ([Example](showVideo.m)).

### Calculating Time Traces
```matlab
timeEvolution = data.getTimeEvolution();
```

Calculates the corresponding time evolution is calculated (like in the top right image) ([Example](showTimeEvolutions.m)).

### Calculate Parameter
```matlab
methods = {@getQuot, @getDiaphragm};
healthy_before = CTData.parameterIterator('CN', 'BC', 1:3, methods);
healthy_after = CTData.parameterIterator('CN', 'AP', 1:3, methods);
ill_before = CTData.parameterIterator('SAA', 'BC', 1:10, methods);
ill_after = CTData.parameterIterator('SAA', 'AP', 1:10, methods);
```
Iterate over multiple data sets and calculate multiple parameter from specified methods ([Example](parameterIteration.m)).

## Multiparametric Classification
By evaluating both the expansion of the diaphragm and the ratio of fall and rise time of the breating signal, it is possible to distinguish healthy (CN) from asthmatic mice (SAA).
![Multiparametric Classification](./images/02_multiparametric_classification.png)
