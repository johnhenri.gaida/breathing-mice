clear all;
raw_data = [CTData.loadData('CN', 'BC',2) CTData.loadData('CN', 'AP',2) CTData.loadData('SAA','BC',8) CTData.loadData('SAA','AP',8)] ;

%% 

min_thresh=800;
max_thresh=1380;
filtered.MF(filtered.MF<min_thresh)=min_thresh;
filtered.MF(filtered.MF>max_thresh)=max_thresh;
%unfiltered.MF(unfiltered.MF<900)=900;
%%
%for frame=1:1024
    unfiltered=raw_data(3);
filtered=unfiltered;
    
for frame=1:1024
         filtered.MF(:,:,frame)=medfilt2(filtered.MF(:,:,frame),[5 5]);
    end
    
   % filtered.MF=medfilt3(filtered.MF,[3 3 3]);
    %subplot(1,2,1);
    %imshow(test,[800 1380]);
    %subplot(1,2,2);
    %imshow(old,[800 1380]);
%end
min_thresh=800;
max_thresh=1420;
filtered.MF(filtered.MF<min_thresh)=min_thresh;
filtered.MF(filtered.MF>max_thresh)=max_thresh;


unfiltered.MF(unfiltered.MF<min_thresh)=min_thresh;
unfiltered.MF(unfiltered.MF>max_thresh)=max_thresh;
playMovie([unfiltered filtered]);



%%
%for frame=1:1024
    unfiltered=raw_data(3);
filtered=unfiltered;
    
for frame=1:1024
         bildFT=fftshift(fft2(unfiltered.MF(:,:,frame)));
         %figure, 
         %subplot(1,3,1)
         %imshow(unfiltered.MF(:,:,frame),[]);
         %subplot(1,3,2)
         %imshow(log(1+abs(bildFT)),[]);
         %subplot(1,3,3)
         mask=mat2gray(fspecial('Gaussian',(size(unfiltered.MF(:,:,frame))),40));
         %mask=1-mask;      
         filteredFT=bildFT.*mask;
         result=(ifft2(ifftshift(filteredFT)));
         %imshow(result,[]);
         filtered.MF(:,:,frame)=real(result);
         
end
%%
min_thresh=800;
max_thresh=1450;
filtered.MF(filtered.MF<min_thresh)=min_thresh;
filtered.MF(filtered.MF>max_thresh)=max_thresh;


unfiltered.MF(unfiltered.MF<min_thresh)=min_thresh;
unfiltered.MF(unfiltered.MF>max_thresh)=max_thresh;
playMovie([unfiltered filtered]);

%%
figure(1);
mappi=@gray;
immap=mappi(1700);
pic=unfiltered.MF(:,:,10);
[counts, Xx]=imhist(pic,immap);
limm=graythresh(pic);
xlim([min(min(pic)) max(max(pic))]);

%%
playMovie(unfiltered);

