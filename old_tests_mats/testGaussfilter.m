%% load random mouse data
% disables warning messanges connected to the TIFF reading
warning('off','MATLAB:imagesci:tiffmexutils:libtiffErrorAsWarning')

data(1) = CTData.loadData('CN', 'BC',1);
data(2) = CTData.loadData('CN','AP',1);
data(3) = CTData.loadData('SAA', 'BC',9);
data(4) = CTData.loadData('SAA', 'AP',9);

%%
screen_size=get(0,'screensize');
            screen_width=screen_size(3);
            screen_height=screen_size(4);
pic=data(1).MF(:,:,10);
fig1=figure(1);
set(fig1,'Position',[10,10,screen_width*0.9,screen_height*0.9]);
subplot(1,2,1);
imshow(pic,[800 1400]);

gauss=fspecial('gaussian',[10 10],1);
filtered=imfilter(pic,gauss);
subplot(1,2,2);
imshow(filtered,[800 1400]);
colormap(jet);
filtereddata=data(1);
filtereddata.MF=imfilter(data(1).MF,gauss);
playMovie([data(1) filtereddata]);

