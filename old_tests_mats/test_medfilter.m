clear all;
raw_data = [CTData.loadData('CN', 'BC',1) CTData.loadData('CN', 'AP',1) CTData.loadData('SAA','BC',9) CTData.loadData('SAA','AP',9)] ;

%% 
unfiltered=raw_data(1);
filtered=unfiltered;
filtered=filtered.setDynRange(800,1380);
%unfiltered.MF(unfiltered.MF<900)=900;
%%
%for frame=1:1024
    for frame=1:1024
         filtered.MF(:,:,frame)=medfilt2(filtered.MF(:,:,frame),[5 5]);
    end
filtered.FilterType = 'medfilt2(...,[3 3])';
  
% filtered.MF=medfilt3(filtered.MF,[3 3 3]);
  
filtered=filtered.setDynRange(800,1300);
unfiltered=unfiltered.setDynRange(800,1300);
playMovie([filtered unfiltered],@parula);

%%
figure(1);
mappi=@gray;
immap=mappi(1700);
pic=unfiltered.MF(:,:,10);
imhist(pic,immap);
%lim=graythresh(pic);
xlim([min(min(pic)) max(max(pic))]);

%%
playMovie(unfiltered);

