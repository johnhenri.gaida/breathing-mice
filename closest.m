function [idx, closest] = closest(val,f)
tmp = abs(f-val);
[idx idx] = min(tmp); %index of closest value
closest = f(idx); %closest value
end