%%
clear all;
raw_data = [CTData.loadData('SAA', 'BC',6) ...
    CTData.loadData('SAA', 'BC',7) ...
    CTData.loadData('SAA','BC',8) ...
    CTData.loadData('SAA','BC',9)] ;

%%
%playMovie(raw_data)
lung_cut=raw_data;
back_cut=raw_data;

%% for all mice
for mouse=1:size(raw_data,2)
    [BW,lung_cut(mouse).MF]=lung_roi3(raw_data(mouse).MF);
    [TW,back_cut(mouse).MF]=bg_roi3(raw_data(mouse).MF);
end

%% cut lung and background for all frames and all mouses


raw_data(3)=raw_data(3).setDynRange(800,1380);
lung_cut(3)=lung_cut(3).setDynRange(800,1380);
back_cut(3)=back_cut(3).setDynRange(600,1680);
playMovie([raw_data(3) lung_cut(3) back_cut(3)])
%% ---OLD---
%% time evolution of integration
color=['r' 'b' 'g' 'k'];
t_fig=figure(1);

screen_size=get(0,'screensize');
            screen_width=screen_size(3);
            screen_height=screen_size(4);
set(t_fig,'Position',[10,10,screen_width*0.9,screen_height*0.9]);
set(t_fig,'Name','Time evolution of integral');
for pl=1:4
    subplot(4,1,pl);
    integrated(:,pl)=squeeze(mean(mean(lung_cut(pl).MF(:,:,:))));
    back_integrate(:,pl)=squeeze(mean(mean(back_cut(pl).MF(:,:,:))));
    
    roi_evolve(:,pl)=(integrated(:,pl)-back_integrate(:,pl))./mean(back_integrate(:,pl));
    back_tend(:,pl)=smooth(roi_evolve(:,pl),200);
    roi_evolve(:,pl)=roi_evolve(:,pl)-back_tend(:,pl);
    %norm_integral(pl)=sum(integrated(:,pl));
    plot(roi_evolve(:,pl),'Color',color(pl));
   % hold on;
    %xlim([200,500]);
    ylim([min(roi_evolve(200:400,1)) max(roi_evolve(200:400,1))]);
    title(getString(lung_cut(pl)));
end






%%  old
f_fig=figure(2);
color=['r' 'b' 'g' 'k'];
set(f_fig,'Position',[10,10,screen_width*0.9,screen_height*0.9]);
set(f_fig,'Name','Temporal Frequency Space');
for pl=1:4
    subplot(4,1,pl);
    four=abs(fftshift(fft(integrated(:,pl))));
    four_cor=four(length(four)/2:length(four));
    dur=1024/30;
    df=1./dur;
    freq=0:df:(length(four)/2)*df;
    plot(freq,four_cor,'r')
    ylim([0 10E7]);
    xlim([0 5]); 
    title(getString(raw_data(pl)));
end
