tic
methods = {@getQuot, @getDiaphragm};
healthy_before = CTData.parameterIterator('CN', 'BC', 1:3, methods);
%%
healthy_after = CTData.parameterIterator('CN', 'AP', 1:3, methods);
%%
ill_before = CTData.parameterIterator('SAA', 'BC', 1:10, methods);
%%
ill_after = CTData.parameterIterator('SAA', 'AP', 1:10, methods);
toc
%%
black = [0 0 0];
darkblue = [53 68 88]./256;
blue = [58 154 217]./256;
blue = [74 90 169]./256;
green = [41 171 164]./256;
green = [0 140 72]./256;
red = [235 114 96]./256;
red = [238 46 47]./256;
msize = 25;
idx1 = 1;
idx2=2;

figure(3)
plot(healthy_before(:,idx1),healthy_before(:,idx2),'.','markersize',msize, 'color', black)
hold on
plot(healthy_after(:,idx1),healthy_after(:,idx2),'b.','markersize',msize, 'color', blue)
plot(ill_before(:,idx1),ill_before(:,idx2),'k.','markersize',msize, 'color', green)
plot(ill_after(:,idx1),ill_after(:,idx2),'r.','markersize',msize, 'color', red)
legend('CN-BC', 'CN-AP', 'SAA-BC', 'SAA-AP')
%xlabel('no'), ylabel('constant offset')
xlabel('Ratio of Rise and Fall Time');
ylabel('Diaphragm Expansion');
%xlim([0 11])
hold off
%%
figure(2)
plot(zeros(1,3),healthy_after(:,idx1)-healthy_before(:,idx1), 'c.','markersize',msize)
hold on
plot(zeros(1,10),ill_after(:,idx1)-ill_before(:,idx1), 'k.','markersize',msize)
hold off

%%
figure(3)
plot(healthy_before(:,1),zeros(1,3),'.','markersize',msize, 'color', black)
hold on
plot(healthy_after(:,1),zeros(1,3)+1,'b.','markersize',msize, 'color', blue)
plot(ill_before(:,1),zeros(1,10)+2,'k.','markersize',msize, 'color', green)
plot(ill_after(:,1),zeros(1,10)+3,'r.','markersize',msize, 'color', red)
%legend('CN-BC', 'CN-AP', 'SAA-BC', 'SAA-AP')
%xlabel('no'), ylabel('constant offset')
xlabel('Offset');
%ylabel('Diaphragm Expansion');
ylim([-0.5 3.5])
set(gca,'ytick',[])
hold off

%%
figure(4)
plot(healthy_after(:,1)-healthy_before(:,1),zeros(1,3),'.','markersize',msize, 'color', black)
hold on
plot(ill_after(:,1)-ill_before(:,1),zeros(1,10)+1,'b.','markersize',msize, 'color', blue)
%legend('CN-BC', 'CN-AP', 'SAA-BC', 'SAA-AP')
%xlabel('no'), ylabel('constant offset')
xlabel('Quotient Fall/Rise Time');
%ylabel('Diaphragm Expansion');
ylim([-0.5 3.5])
set(gca,'ytick',[])
hold off
