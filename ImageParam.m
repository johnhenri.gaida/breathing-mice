classdef ImageParam
    % Image parameters
    
    properties
        PixelSize
    end
    
    methods
        function obj = ImageParam(pixelSize)
            obj.PixelSize = pixelSize;
        end
        
        function length = getLength(obj, pixels)
            % calculate length for a given number of pixels
            length = pixels * obj.PixelSize;
        end
        
    end
        
end