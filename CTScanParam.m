classdef CTScanParam
    % CT scan parameters
    
    properties
        TubePotential   % (kV)
        TubeCurrent     % (uA)   
    end
    
    methods
        function obj = CTScanParam(tubePotential, tubeCurrent)
            obj.TubePotential = tubePotential;
            obj.TubeCurrent = tubeCurrent;
        end
    end
        
end