obj = CTData.loadData('CN', 'AP',1);
%obj = CTData.loadData('SAA','AP', 3);
%%
avg = mean(obj.MF,3);
%%
deviation = std(double(obj.MF),0,3);
%%
figure(1)
imshow(avg,[]), title([obj.getString,' Averaged image'])
colorbar
figure(3)
imshow(deviation,[10.13 68]), title([obj.getString,sprintf(' Standard deviation = %.1f',mean(mean(deviation)))])
colormap(parula)
colorbar
