classdef MFParam
    % Multifram parameters
    
    properties
        Fps     % Framerate
    end
    
    methods
        function obj = MFParam(fps)
            obj.Fps = fps;
        end
    end
        
end