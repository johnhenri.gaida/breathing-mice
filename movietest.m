%% load random mouse data
% disables warning messanges connected to the TIFF reading


data(1) = CTData.loadData('CN', 'BC',1);
data(2) = CTData.loadData('SAA','AP',4);
data(3) = CTData.loadData('SAA', 'BC',9);
data(4) = CTData.loadData('SAA', 'AP',9);
%% some movie generations.... first one single movie
playMovie(data(1), @hot);

%% now two next to each other
playMovie(data(3:4), @jet);

%% and 3
playMovie(data(1:3), @parula);

%% and let's check how a fourth column looks
playMovie(data(1:4));

%% and now... close all movie windows which tend to stay open and steal memory
close all hidden;