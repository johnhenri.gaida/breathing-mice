clear all;
raw_data = [CTData.loadData('SAA', 'BC',4) CTData.loadData('SAA', 'AP',4) CTData.loadData('SAA','BC',8) CTData.loadData('SAA','AP',8)] ;


%% time evolution of integration
crop_data=raw_data;
screen_size=get(0,'screensize');
            screen_width=screen_size(3);
            screen_height=screen_size(4);

color=['r' 'b' 'g' 'k'];
t_fig=figure(1);
set(t_fig,'Position',[10,10,screen_width*0.9,screen_height*0.9]);
set(t_fig,'Name','Time evolution of integral');
for pl=1:4
    subplot(4,1,pl);
    crop_data(pl).MF = crop_data(pl).MF(150:300,130:200,:);
    integrated(:,pl)=squeeze(sum(sum(crop_data(pl).MF(:,:,:))));
    norm_integral(pl)=sum(integrated(:,pl));
    plot(integrated(:,pl)/norm_integral(pl),'Color',color(pl));
    xlim([50,1000]);
    ylim([min(integrated(:,pl)/norm_integral(pl)) max(integrated(:,pl)/norm_integral(pl))]);
    title(getString(crop_data(pl)));
end




%%  old
f_fig=figure(2);
color=['r' 'b' 'g' 'k'];
set(f_fig,'Position',[10,10,screen_width*0.9,screen_height*0.9]);
set(f_fig,'Name','Temporal Frequency Space');
for pl=1:4
    subplot(4,1,pl);
    four=abs(fftshift(fft(integrated(:,pl))));
    four_cor=four(length(four)/2:length(four));
    dur=1024/30;
    df=1./dur;
    freq=0:df:(length(four)/2)*df;
    plot(freq,four_cor,'r')
    %ylim([0 2.5E7]);
    xlim([0.25 2]); 
    title(getString(crop_data(pl)));
end
