function int = getInt(startp, endp, array)
int(1) = closest(startp, array);
int(2) = closest(endp, array);
int = sort(int);
end